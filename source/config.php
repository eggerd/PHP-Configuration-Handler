<?php

/**
 * Configuration for the production environment, that contains all existing settings.
 * Settings that are expected to be defined by the secrets.php should be declared as follows,
 * so that an exception will be thrown, if the setting does not get defined.
 *
 * config::set(config::secret('SETTING_NAME'));
 */

config::set('VERSION', '1.2.3');
config::set(config::secret('PASSWORD'));
config::set('USERS', array('Foo', 'Bar'));

// the following settings will be declared as global variables, because their value isn't scalar
config::set('DATE', new DateTime('1995-06-08'));
config::set('HOLIDAYS', array(new DateTime('2019-01-01')));
config::set('ORDERS', array('John Doe', 'dates' => array(new DateTime('2012-12-12'))));

?>