<?php

/**
 * File to define settings with confidential values. This file overwrites all other configurations.
 * This file should only be present on the environment where it is needed and it should not be
 * pushed to your repository!
 */

config::set('PASSWORD', 'secretpassword');

?>