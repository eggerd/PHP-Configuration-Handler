<?php

// Copyright (c) 2018 Dustin Eckhardt
// v2.4.1 - 06.09.2019

/**
 * The configuration process is automatically started after this file has been included, due to the
 * execution of the following method. This behavior can be disabled by declaring a constant with
 * the name "CONFIG_DISABLE_AUTORUN", before including this file.
 *
 * The first parameter of this method should be overwritten during deploy, depending on the environment
 */
if(!defined('CONFIG_DISABLE_AUTORUN'))
{
	config::run('local');
}



/**
 * Manages the declaration of settings, that are defined by calling set(). Settings that have already
 * been defined, by calling set(), will not be overwritten, which allow multiple declarations that
 * can be loaded by priority (environment configurations).
 *
 * Note that settings will be declared as global variables, if their value isn't a scalar (integer,
 * float, string, boolean or null) or an array of scalars.
 *
 * When all settings have been defined, calling finalize() will declare the final constants and the
 * configuration can not be changed anymore. To see which file declared (or tried to) a setting or
 * which values are applied, call debug()
 */
final class config
{
	/** @var array = storage for all settings getting defined by calling set() */
	private static $heap = array();

	/** @var boolean = will be toggled if finalize() is called */
	private static $finalized = false;



	/**
	 * Runs the configuration by including all necessary configuration files, depending on the
	 * specified environment, and finalizing all settings
	 *
	 * @param string $environment = {'prod', 'test', 'local'} = name of the environment
	 * @return void
	 */
	public static function run($environment)
	{
		config::environment($environment); // set the environment constant

		/**
		 * This file contains secret settings, like passwords for the production environment, that
		 * will not be deployed along with a project and its regular config files itself
		 */
		config::include_file('secrets.php');

		// load configuration for one of the test environments, if required
		if(CONFIG_ENVIRONMENT === 'test')
		{
			config::include_file('config.test.php');
		}
		elseif(CONFIG_ENVIRONMENT === 'local' || (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] === '::1'))
		{
			config::include_file('config.local.php');
		}

		/**
		 * Always load the configuration file for the production environment, since the other config
		 * files only define settings that have to be different from the production environment. So
		 * all remaining settings are then loaded from the production environments config file
		 */
		config::require_file('config.php');
		config::finalize();
	}



	/**
	 * Writes the passed setting to the heap with some debug information. After a setting as been
	 * defined it will not be overwritten, if passed again with a different value! If the value of
	 * a setting isn't a scalar (integer, float, string, boolean or null) or an array of scalars, it
	 * will be declared as global variable instead of a constant
	 *
	 * @param string $key = the name of this setting to use for the constant (will be transformed into uppercase)
	 * @param mixed $value = the value of this setting
	 * @return void
	 *
	 * @throws Exception Trying to declare '$KEY' after configuration has been finalized!
	 */
	public static function set($key, $value = null)
	{
		if(config::$finalized === true)
		{
			throw new Exception("Trying to declare '".$key."' after configuration has been finalized!");
		}

		$key = strtoupper($key);
		$debug = debug_backtrace();

		if(isset(config::$heap[$key]))
		{
			// save the path of the files, which also tried to define this setting
			config::$heap[$key]['alternatives'][] = $debug[0]['file'];
			return;
		}

		// saving the path of the file, this function has been called from
		config::$heap[$key]['file'] = $debug[0]['file'];
		config::$heap[$key]['value'] = $value;
	}



	/**
	 * Get the value of an already defined setting. Can be used for cross-referencing settings, but
	 * settings have to be in correct order (referenced setting has to be already defined)!
	 *
	 * @param string $key = the name of a setting
	 * @return array {'defined' => boolean, ['value' => mixed]}
	 */
	public static function get($key)
	{
		$key = strtoupper($key);
		if(isset(config::$heap[$key]))
		{
			return array('defined' => true, 'value' => config::$heap[$key]['value']);
		}

		return array('defined' => false);
	}



	/**
	 * Should be used on every setting that is supposed to be defined by the secrets.php file, in
	 * order to check if it actually has been defined or not. If the settings has not been defined
	 * at the moment this function is called, an exception will be thrown
	 *
	 * Usage: config::set(config::secret('key'), null)
	 *
	 * @param string $key = the name of a setting
	 * @return string = the value passed with $key
	 *
	 * @throws Exception Configuration failed! Missing secret for '$KEY'!
	 */
	public static function secret($key)
	{
		$key = strtoupper($key);
		if(!isset(config::$heap[$key])) // if setting hasn't been defined through secrets.php or test config yet
		{
			throw new Exception("Configuration failed! Missing secret for '".$key."'!");
		}

		config::$heap[$key]['secret'] = true;
		return $key;
	}



	/**
	 * Validates the environment name and declares the CONFIG_ENVIRONMENT constant. An exception will
	 * be thrown, if an invalid name is passed or the constant is already defined
	 *
	 * @param string $name = {'prod', 'test', 'local'} = name of the environment
	 * @return void
	 *
	 * @throws Exception Configuration failed! Environment has already been set to '$NAME', can't set it to '$NEW_NAME'!
	 * @throws InvalidArgumentException Configuration failed! Invalid environment name '$NAME'!
	 */
	public static function environment($name)
	{
		if(!defined('CONFIG_ENVIRONMENT'))
		{
			if(array_search(strtolower($name), array('prod', 'test', 'local')) === false) // if an invalid name is passed
			{
				throw new InvalidArgumentException("Configuration failed! Invalid environment name '".$name."'!");
			}

			define('CONFIG_ENVIRONMENT', $name);
		}

		if(CONFIG_ENVIRONMENT != $name)
		{
			throw new Exception("Configuration failed! Environment has already been set to '".CONFIG_ENVIRONMENT."', can't set it to '".$name."'!");
		}
	}



	/**
	 * Finalizes all settings saved in $heap by turning them into constants. If the value of a setting
	 * isn't a scalar (integer, float, string, boolean or null) or an array of scalars, a global variable
	 * will be created instead
	 *
	 * @return void
	 *
	 * @throws Exception Configuration has already been finalized!
	 * @throws Exception Configuration failed! Constant '$KEY' has already been defined!
	 */
	public static function finalize()
	{
		if(config::$finalized === true) // only if this functions has not already been executed
		{
			throw new Exception("Configuration has already been finalized!");
		}

		foreach (config::$heap as $key => &$meta)
		{
			if(is_array($meta['value']))
			{
				// check whether all values of the array are scalar, so that it can be declared as constant
				$meta['scalar'] = true;
				array_walk_recursive($meta['value'], function($value, $key) use (&$meta)
				{
					if($meta['scalar'] && !is_scalar($value) && !is_null($value))
					{
						$meta['scalar'] = false;
					}
				});
			}

			if(is_scalar($meta['value']) || is_null($meta['value']) || (is_array($meta['value']) && $meta['scalar']))
			{
				if(defined($key)) // if this key is already used by a constant
				{
					throw new Exception("Configuration failed! Constant '".$key."' has already been defined!");
				}

				define($key, $meta['value']);
				continue;
			}

			// all settings that don't have a scalar value will be declared as global variables
			global ${$key};
			${$key} = $meta['value'];
		}

		config::$finalized = true;
	}



	/**
	 * Includes the file specified in $filename, if it exists and is readable. The path to this
	 * functions file will be used as base path
	 *
	 * @param string $filename = name of the file to include
	 * @return boolean = if the file was included
	 */
	public static function include_file($filename)
	{
		if(is_readable(dirname(__FILE__).'/'.$filename))
		{
			require_once(dirname(__FILE__).'/'.$filename);
			return true;
		}

		return false;
	}


	/**
	 * Includes the file specified in $filename. An exception will be thrown, if this file doesn't
	 * exist or isn't readable. The path to this functions file will be used as base path
	 *
	 * @param string $filename = name of the file to include
	 * @return void
	 *
	 * @throws RuntimeException Configuration failed! Missing '$FILENAME'!
	 */
	public static function require_file($filename)
	{
		if(!config::include_file($filename))
		{
			throw new RuntimeException("Configuration failed! Missing '".$filename."'");
		}
	}



	/**
	 * Check if the configuration has already been finalized
	 *
	 * @return boolean
	 */
	public static function status()
	{
		return config::$finalized;
	}



	/**
	 * Prints all defined settings and their values, as well as the paths to all files that (tried)
	 * to define those settings, in the order they called set(). Values of secret settings will not
	 * be visible. The name of the currents environment and whether the configuration has been
	 * finalized will be printed as well.
	 *
	 * Can not be used on the production environment, to prevent accidental leaking of possibly
	 * sensitive information!
	 *
	 * @return void
	 */
	public static function debug()
	{
		if(CONFIG_ENVIRONMENT != 'prod')
		{
			$settings = config::$heap;
			foreach ($settings as $key => $meta)
			{
				if(isset($meta['secret']) && $meta['secret'] === true)
				{
					$settings[$key]['value'] = '********';
				}
			}

			$dump = array(
				'ENVIRONMENT' => defined('CONFIG_ENVIRONMENT') ? CONFIG_ENVIRONMENT : 'undefined',
				'FINALIZED' => config::$finalized,
				'SETTINGS' => $settings);

			echo '<pre>'; var_dump($dump); echo '</pre>';
		}
	}
}

?>
