<?php

/**
 * Configuration for your localhost environment.
 * Only define settings that have to be different from the production environment!
 */

config::set('VERSION', '0.0.0');
config::set('PASSWORD', null);

?>