<?php

/**
 * Prevents configuration automatically being finalized upon including "configure.php", in order to
 * manually run the configuration, depending on the options selected on this demo page
 */
define('CONFIG_DISABLE_AUTORUN', true);
require(realpath(dirname(__FILE__).'/../source/configure.php'));


/**
 * Updating demo options according to selection
 */
$demo = (object) ['environment' => 'test', 'secrets' => true];
if(isset($_REQUEST['submit']))
{
	isset($_REQUEST['environment']) ? $demo->environment = $_REQUEST['environment'] : null;
	isset($_REQUEST['secrets']) ? $demo->secrets = ($_REQUEST['secrets'] === 'true') : null;
}

?>

<html>
<head>
	<style>
		body {margin: 0; font-family: Tahoma;}
		form {text-align: center; background-color: lightgray; padding: 10px;}
		form div {display: inline-block;}
		form p {font-size: 10px; margin: 0 0 3px 0; text-align: left; padding-left: 7px;}
		form select {padding: 2px;}
		form input {padding: 2px 6px;}
		pre {width: fit-content; margin: 0 auto;}
	</style>
</head>
<body>
	<form>
		<div>
			<p>Environment</p>
			<select name="environment">
				<option value="prod" <?php echo $demo->environment == 'prod' ? 'selected' : null ?>>Production</option>
				<option value="test" <?php echo $demo->environment == 'test' ? 'selected' : null ?>>Testing</option>
				<option value="local" <?php echo $demo->environment == 'local' ? 'selected' : null ?>>Localhost</option>
			</select>
		</div>
		<div>
			<p>Secrets</p>
			<select name="secrets">
				<option value="true" <?php echo $demo->secrets == true ? 'selected' : null ?>>Include</option>
				<option value="false" <?php echo $demo->secrets == false ? 'selected' : null ?>>Don't include</option>
			</select>
		</div>
		<input type="submit" name="submit" value="Apply" />
	</form>
	<pre>
	<?php

		try
		{
			/**
			 * The following steps are normally performed by config::run(), which would automatically
			 * include configuration files depending on the specified environment.
			 *
			 * But since "secrets.php" is always included, if the file exists, these steps are
			 * performed manually for this demo page, in order to be able to disable the inclusion
			 * of "secrets.php"
			 */
			config::environment($demo->environment);
			$demo->secrets ? config::require_file('secrets.php') : null;
			$demo->environment == 'test' ? config::include_file('config.test.php') : null;
			$demo->environment == 'local' ? config::include_file('config.local.php') : null;
			config::require_file('config.php');
			config::finalize();

			/**
			 * Prints debug output for all declared settings, only if the environment is not set
			 * to "prod"!
			 */
			echo '<br />----- Debug Output -----<br /><br />';
			config::debug();

			/**
			 * Manually print the value of some settings, which will work regardless of the current
			 * environment
			 */
			echo '<br />----- Printing Settings -----<br /><br />';
			var_dump(VERSION);
			var_dump(PASSWORD);
			var_dump(USERS);
			var_dump($DATE);
			var_dump($HOLIDAYS);
			var_dump($ORDERS);
		}
		catch(Exception $e)
		{
			/**
			 * An exception will be thrown, if the environment of the demo is set to "prod" but
			 * secrets are not included, because the config for the production environment declared
			 * some settings as secrets (see "config.php")
			 */
			echo '<br />----- Exception Thrown -----<br /><br />'.$e->getMessage();
		}

	?>
	</pre>
</body>
</html>