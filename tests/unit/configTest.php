<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;

defined('CONFIG_DISABLE_AUTORUN') ? null : define('CONFIG_DISABLE_AUTORUN', true);
require(realpath(dirname(__FILE__).'/../../source/configure.php'));

/**
 * @runTestsInSeparateProcesses
 */
final class configTest extends Testcase
{
    /**
     * This test depends on the values defined in configurations files that are saved in the source
     * folder! This test validates that settings are defined as expected during the execution of run()
     *
     * @test
     * @covers config::run()
     */
    public function runValidateTestEnvExample()
    {
        global $DATE, $HOLIDAYS, $ORDERS;

        config::run('test');

        $this->assertEquals('1.2.3', VERSION);
        $this->assertEquals('secretpassword', PASSWORD);
        $this->assertEquals(array('Foo', 'Bar'), USERS);
        $this->assertEquals((new DateTime('1995-06-08'))->getTimestamp(), $DATE->getTimestamp());
        $this->assertEquals((new DateTime('2019-01-01'))->getTimestamp(), $HOLIDAYS[0]->getTimestamp());
        $this->assertEquals((new DateTime('2012-12-12'))->getTimestamp(), $ORDERS['dates'][0]->getTimestamp());
    }



    /**
     * This test depends on the values defined in configurations files that are saved in the source
     * folder! This test validates that settings are defined as expected during the execution of run()
     *
     * @test
     * @covers config::run()
     */
    public function runValidateLocalhostEnvExample()
    {
        config::run('local');

        $this->assertEquals('0.0.0', VERSION);
        $this->assertEquals('secretpassword', PASSWORD);
    }



    /**
     * This test depends on the values defined in configurations files that are saved in the source
     * folder! This test validates that settings are defined as expected during the execution of run()
     *
     * @test
     * @covers config::run()
     */
    public function runAutoloadLocalhostEnv()
    {
        $_SERVER['REMOTE_ADDR'] = '::1';
        config::run('prod');

        $this->assertEquals('0.0.0', VERSION);
        $this->assertEquals('secretpassword', PASSWORD);
    }



    /**
     * An exception should be thrown, if the configuration already has been finalized, but a setting
     * is about to be defined
     *
     * @test
     * @covers config::set()
     */
    public function setAlreadyFinalized()
    {
        $this->expectExceptionMessageMatches("/^Trying to declare '.*' after configuration has been finalized/");

        config::finalize();
        config::set('SETTING_KEY', 'value');
    }



    /**
     * The value of an already defined setting should not be overwritten, if set() is called again
     * for the same setting
     *
     * @test
     * @covers config::set()
     */
    public function setFirstInFirstOut()
    {
        config::set('SETTING_KEY', 'first');
        config::set('SETTING_KEY', 'second');

        $this->assertEquals('first', config::get('SETTING_KEY')['value']);
    }



    /**
     * Get value of already defined setting
     *
     * @test
     * @covers config::get()
     */
    public function getDefinedSetting()
    {
        config::set('SETTING_KEY', 'Value_123');
        $return = config::get('setting_key'); // note the different case sensitivity

        $this->assertTrue($return['defined']);
        $this->assertEquals('Value_123', $return['value']);
    }



    /**
     * Trying to get value of undefined setting
     *
     * @test
     * @covers config::get()
     */
    public function getUndefinedSetting()
    {
        $this->assertFalse(config::get('UNDEFINED_SETTING_KEY')['defined']);
    }



    /**
     * No exception should be thrown, if the secret was defined
     *
     * @test
     * @covers config::secret()
     */
    public function secretGotDefined()
    {
        config::set('SECRET_SETTING_KEY', 'some_value');
        $this->assertEquals('SECRET_SETTING_KEY', config::secret('secret_setting_key')); // note the different case sensitivity
    }



    /**
     * An exception should be thrown, if a secret has not been defined
     *
     * @test
     * @covers config::secret()
     */
    public function secretUndefined()
    {
        $this->expectExceptionMessageMatches("/^Configuration failed\! Missing secret/");
        config::secret('SECRET_SETTING_KEY');
    }



    /**
     * An exception should be thrown, if the environment constant has already been set to a
     * different value
     *
     * @test
     * @covers config::environment()
     */
    public function environmentAlreadyDefined()
    {
        $this->expectExceptionMessageMatches("/^Configuration failed\! Environment has already been set/");

        define('CONFIG_ENVIRONMENT', 'prod');
        config::environment('local');
    }



    /**
     * No exception should be thrown, if the environment constant has already been set to a
     * different value, but it's the same as the one passed to the function
     *
     * @test
     * @covers config::environment()
     */
    public function environmentAlreadyDefinedButEqual()
    {
        define('CONFIG_ENVIRONMENT', 'prod');
        config::environment('prod');

        $this->assertTrue(true); // no exception thrown
    }



    /**
     * An exception should be thrown, if an invalid environment name is passed
     *
     * @test
     * @covers config::environment()
     */
    public function environmentInvalidName()
    {
        $this->expectExceptionMessageMatches("/^Configuration failed\! Invalid environment name/");
        config::environment('invalid name');
    }



    /**
     * Environment constant should be set to the passed environment name
     *
     * @test
     * @covers config::environment()
     */
    public function environmentSetSuccessfully()
    {
        config::environment('test');
        $this->assertEquals('test', CONFIG_ENVIRONMENT);
    }



    /**
     * An exception should be thrown, if the config is already finalized when calling finalize()
     *
     * @test
     * @covers config::finalize()
     */
    public function finalizeAlreadyFinalized()
    {
        $this->expectExceptionMessageMatches("/^Configuration has already been finalized/");

        config::finalize();
        config::finalize();
    }



    /**
     * An exception should be thrown, if a constant with the same name as a setting key is already
     * defined
     *
     * @test
     * @covers config::finalize()
     */
    public function finalizeSettingAlreadyDefined()
    {
        $this->expectExceptionMessageMatches("/^Configuration failed! Constant '.*' has already been defined/");

        define('SETTING_KEY', 'value');
        config::set('SETTING_KEY', 'value');
        config::finalize();
    }



    /**
     * Settings whose value isn't a scalar (integer, float, string, boolean or null) or an array of
     * scalars, should be declared as global variable instead
     *
     * @test
     * @covers config::finalize()
     */
    public function finalizeValidateSettings()
    {
        global $SETTING_KEY_OBJECT, $SETTING_KEY_ARRAY_OBJECT;

        config::set('SETTING_KEY_INTEGER', 1);
        config::set('SETTING_KEY_FLOAT', 1.99);
        config::set('SETTING_KEY_BOOLEAN', true);
        config::set('SETTING_KEY_NULL', null);
        config::set('SETTING_KEY_STRING', 'string');
        config::set('SETTING_KEY_ARRAY', array(1, 2, 3));
        config::set('SETTING_KEY_OBJECT', new DateTime('1999-01-01 10:10', new DateTimeZone('Europe/Berlin')));
        config::set('SETTING_KEY_ARRAY_OBJECT', array(new DateTime('1999-01-01 10:10', new DateTimeZone('Europe/Berlin'))));
        config::finalize();

        $this->assertEquals(1, SETTING_KEY_INTEGER);
        $this->assertEquals(1.99, SETTING_KEY_FLOAT);
        $this->assertEquals(true, SETTING_KEY_BOOLEAN);
        $this->assertEquals(null, SETTING_KEY_NULL);
        $this->assertEquals('string', SETTING_KEY_STRING);
        $this->assertEquals(array(1, 2, 3), SETTING_KEY_ARRAY);
        $this->assertEquals('1999-01-01 10:10', $SETTING_KEY_OBJECT->format('Y-m-d H:i'));
        $this->assertEquals('1999-01-01 10:10', $SETTING_KEY_ARRAY_OBJECT[0]->format('Y-m-d H:i'));
    }



    /**
     * Including a readable file should return TRUE
     *
     * @test
     * @covers config::include_file()
     */
    public function includeReadableFile()
    {
        $this->assertTrue(config::include_file('config.test.php'));
    }



    /**
     * Including an unreadable file should return FALSE
     *
     * @test
     * @covers config::include_file()
     */
    public function includeUnreadableFile()
    {
        $this->assertFalse(config::include_file('nonexisting.file'));
    }



    /**
     * No exception should be thrown, if a required file is Successfully included
     *
     * @test
     * @covers config::require_file()
     */
    public function requireReadableFile()
    {
        config::require_file('secrets.php');
        $this->assertTrue(true); // no exception thrown
    }



    /**
     * An exception should be thrown, if a required file is unreadable
     *
     * @test
     * @covers config::require_file()
     */
    public function requireUnreadableFile()
    {
        $this->expectExceptionMessageMatches("/^Configuration failed\! Missing/");
        config::require_file('nonexisting.file');
    }



    /**
     * Status should be FALSE, if config has not been finalized
     *
     * @test
     * @covers config::status()
     */
    public function statusNotFinalized()
    {
        $this->assertFalse(config::status());
    }



    /**
     * Status should be TRUE, if config has been finalized
     *
     * @test
     * @covers config::status()
     */
    public function statusFinalized()
    {
        config::finalize();
        $this->assertTrue(config::status());
    }



    /**
     * Debug output should not be printed when running on production environment
     *
     * @test
     * @covers config::debug()
     */
    public function debugNotOnProduction()
    {
        define('CONFIG_ENVIRONMENT', 'prod');

        $this->expectOutputString('');
        config::debug();
    }



    /**
     * Debug output should contain information about the used environment, status and all defined
     * settings
     *
     * @test
     * @covers config::debug()
     */
    public function debugValidateOutput()
    {
        define('CONFIG_ENVIRONMENT', 'test');
        config::set('SETTING_KEY', 'first_defined_value');
        config::set('SETTING_KEY', 'second_defined_value');
        config::set('SETTING_KEY', 'third_defined_value');
        config::set('SECRET_SETTING_KEY', 'secret_value');
        config::secret('SECRET_SETTING_KEY');

        $file = preg_quote(__FILE__, '/');
        $regex = '/ENVIRONMENT.*test.*FINALIZED.*false.*SETTINGS.*SETTING_KEY.*file.*'.$file;
        $regex .= '.*value.*first_defined_value.*alternatives.*0.*'.$file.'.*1.*'.$file;
        $regex .= '.*SECRET_SETTING_KEY.*file.*'.$file.'.*value.*\*{8}.*secret.*true/sU';

        $this->expectOutputRegex($regex);
        config::debug();
    }
}