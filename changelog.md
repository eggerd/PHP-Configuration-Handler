2.4.1 - 06.09.2019
------------------
- Arrays that contain non-scalar values (e.g. objects) will be declared as global variables


2.4.0 - 29.03.2019
------------------
- **Issue** #2: Settings with an array as value will now be declared as constants, too, just like scalar types
- At least PHP 7.0 is now required
- Fixed some minor typos


2.3.1 - 14.06.2018
------------------
- The project is now licensed under MIT Expat
- Added readme file


2.3.0 - 11.05.2018
------------------
- The `config::class` now also handles the inclusion of necessary files and validation of the environment
- The configuration is now started by calling `config::run()` with the name of the environment as first parameter. CI/CD configurations have to be adjusted accordingly, to replace the default invocation in `configure.php`
- There will no longer be an exception thrown, if `config.local.php` or `config.test.php` is missing
- Added unit tests for all methods of `config::class`
- Objects can now be defined as settings and will be declared as global variables, just like arrays are
- Added thrown exceptions to DocBlock documentation of functions and updated some other comments
- Restructured project files in order to separate source files from organizational files
- Rewrote demo page and improved visualization


2.2.2 - 03.02.2018
------------------
- Revised the syntax of PHP code
- Rewrote a lot of comments and added DocBlocks to all functions


2.2.1 - 25.07.2017
------------------
- The default environment is now set to `local`
- The value of confidential settings will be displayed as `********` when using `config::debug()`
- `config::debug()` will no longer print any output when used on production environment
- In favor of secret settings the value parameter of `config::set()` is now optional, default is `null`
- Fixed incorrect labels in `config::debug()` output


2.2.0 - 20.07.2017
------------------
- **Issue** #1: Added a gitlab-ci to deploy the project as demo/test version
- A changelog in markdown format has been added
- Some comments have been updated and `$stack` has been renamed to a more accurate name, `$heap`
- The config is now automatically finalized after `config.php` has been loaded
- Fixed a bug where the localhost environment was loaded alongside the test environment, even so only the test environment was expected to load
- The name of the used environment is now included in the debug output
- If a `secrets.php` file is available it will now be loaded at first. It can be used for confidential settings [(documentation)](https://gitlab.com/eggerd/PHP-Configuration-Handler/-/wikis/home#handling-confidential-values)
- Settings that are loaded from `secrets.php` now also have a `['secret'] => true` entry, for debug purposes
- The new function `config::secret()` checks if a settings has already been declared and throws an exception if not. This function is intended to be used on settings that are supposed to be declared by `secrets.php` [(documentation)](https://gitlab.com/eggerd/PHP-Configuration-Handler/-/wikis/home#handling-confidential-values)
- Added example config-, secrets- and index-file to the project
- Added a small form to the example index, so that you can test the different environments and also choose to include the `secrets.php` or not


2.1.1 - 25.05.2017
------------------
- Last version before changelog was added